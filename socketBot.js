'use strict';

module.exports = create;

function create(settings, services) {
  const io = services.socketIO;

  return { register };

  function register(socket, roomName) {
    const receiver = socket;
    const sender = io.in(roomName);

    const wrappedOnMessage = message => onMessage(sender, message);
    receiver.on('message', wrappedOnMessage);

    function deregister() {
      receiver.removeListener('message', wrappedOnMessage);
    }

    function onMessage(sender, message) {
      setTimeout(() => {
        sender.emit('message', {
          owner: 'Stina',
          type: 'text',
          value: "Hi, I'm a bot"
        });
      }, 1000);
    }
  }
}
