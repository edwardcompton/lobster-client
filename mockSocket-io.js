const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const uuid = require('uuid');

const socketBot = require('./socketBot')(null, { socketIO: io });

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
  console.log('a user connected', socket.id);
  const userID = uuid().substring(0, 5);
  const roomName = 'my-room';

  io.in(roomName).emit('message', {
    owner: 'system',
    value: `${userID} joined the chat`
  });
  socket.join(roomName);

  socketBot.register(socket, roomName);

  socket.on('message', message => {
    message.owner = userID;
    socket.to('my-room').emit('message', message);
  });
});

http.listen(3000, function() {
  console.log('listening on *:3000');
});
