import Button from './Button';

const styles = {
  generic: {
    marginLeft: '-10px',
    marginRight: '-10px',
    overflowX: 'scroll',
    scrollSnapType: 'x mandatory'
  },
  scroll: {
    display: 'flex'
  },
  carouselItem: {
    scrollSnapAlign: 'start',
    paddingRight: '10px'
  },
  element: {
    width: '100%',
    border: '1px solid',
    borderColor: '#EEEEEE',
    borderRadius: '10px',
    overflow: 'hidden'
  },
  title: {
    color: '#4b4f56',
    fontWeight: 'bold',
    margin: '8px 18px'
  },
  content: {
    margin: '0 18px',
    marginBottom: '8px',
    color: '#AAAAAA'
  }
};

export default function Generic({ elements }) {
  return (
    <div style={styles.generic} className="lobster-client--hideScrollbar">
      <div
        style={{
          ...styles.scroll,
          width: `calc(90% * ${elements.length})`
        }}
      >
        {elements.map((element, index) => (
          <div
            style={{
              ...styles.carouselItem,
              paddingLeft: index === 0 ? '10px' : 0
            }}
          >
            <Element element={element} />
          </div>
        ))}
      </div>
    </div>
  );
}

function Element({ element }) {
  const buttons = element.buttons || [];

  return (
    <div style={styles.element}>
      <div style={styles.title}>{element.title}</div>
      <div style={styles.content}>{element.content}</div>
      {buttons.map(button => (
        <Button {...button} />
      ))}
    </div>
  );
}
