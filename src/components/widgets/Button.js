import useHover from '../../hooks/useHover';
import { connect } from 'react-redux';

const styles = {
  button: {
    display: 'block',
    width: '100%',
    textAlign: 'center',
    borderTop: '1px solid',
    borderColor: '#EEEEEE',
    textDecoration: 'none',
    padding: '6px',
    fontWeight: 'bold',
    transition: 'background-color 75ms, color 75ms'
  },
  hoveredButton: {
    color: 'white'
  }
};

function Button({ url, title, theme }) {
  const [hoveredRef, isHovered] = useHover();
  const buttonStyles = isHovered
    ? {
        ...styles.button,
        ...styles.hoveredButton,
        backgroundColor: theme.primaryColor
      }
    : {
        ...styles.button,
        color: theme.primaryColor
      };

  return (
    <a style={buttonStyles} href={url} ref={hoveredRef}>
      {title}
    </a>
  );
}

const ConnectedButton = connect(state => ({
  theme: state.theme
}))(Button);
export default ConnectedButton;
