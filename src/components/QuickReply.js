import useHover from '../hooks/useHover';
import { connect } from 'react-redux';

const styles = {
  quickReply: {
    border: '1px solid',
    borderColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    padding: '6px 17px',
    borderRadius: '20px',
    fontWeight: 'bold',
    minWidth: '200px'
  },
  hoveredQuickReply: {
    color: 'white'
  }
};

function QuickReply({ title, onClick, style, theme }) {
  const [hoverRef, isHovered] = useHover();

  const quickReplyStyle = isHovered
    ? {
        ...styles.quickReply,
        ...style,
        ...styles.hoveredQuickReply,
        backgroundColor: theme.primaryColor,
        borderColor: theme.primaryColor
      }
    : {
        ...styles.quickReply,
        ...style,
        color: theme.primaryColor
      };

  return (
    <button ref={hoverRef} onClick={onClick} style={quickReplyStyle}>
      {title}
    </button>
  );
}

export default connect(state => ({ theme: state.theme }))(QuickReply);
