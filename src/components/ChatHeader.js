import { connect } from 'react-redux';

import { minimise } from '../reducers/route';
import ActionButton from './ActionButton';
import Avatar from './Avatar';

const styles = {
  header: {
    display: 'flex',
    backgroundColor: '#2b79d9',
    color: '#fff',
    padding: '10px',
    alignItems: 'center',
    boxShadow: '0 2px 4px rgba(0,0,0,0.2)'
  },
  headerText: {
    fontWeight: 500,
    fontSize: '1.2em'
  },
  avatar: {
    margin: '5px',
    marginRight: '10px',
    height: '25px',
    width: '25px'
  },
  actions: {
    marginLeft: 'auto'
  }
};

function ChatHeader({ theme, style, dispatch }) {
  return (
    <div
      style={{
        ...styles.header,
        backgroundColor: theme.primaryColor,
        ...style
      }}
    >
      <Avatar style={styles.avatar} />
      <div style={styles.headerText}>Stina</div>
      <div style={styles.actions}>
        <ActionButton onClick={() => dispatch(minimise())}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="times"
            class="svg-inline--fa fa-times fa-w-11"
            role="img"
            viewBox="0 0 352 512"
            width="100%"
            height="100%"
          >
            <path
              fill="currentColor"
              d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"
            />
          </svg>
        </ActionButton>
      </div>
    </div>
  );
}

const ConnectedChatHeader = connect(state => ({ theme: state.theme }))(
  ChatHeader
);
export default ConnectedChatHeader;
