import { connect } from 'react-redux';

import { maximise } from '../reducers/route';

const styles = {
  launcher: {
    position: 'fixed',
    bottom: '30px',
    right: '30px',
    margin: '10px',

    cursor: 'pointer',
    height: '60px',
    width: '60px',
    borderRadius: '6px',
    backgroundColor: 'blue',
    boxShadow: 'rgba(0, 0, 0, 0.28) 0px 5px 15px',
    padding: '15px',
    color: 'white',
    opacity: 0,
    animation: 'lobster-client--slide-up 75ms',
    animationFillMode: 'forwards'
  },
  launcherHiding: {
    animation: 'lobster-client--slide-down 75ms',
    animationFillMode: 'forwards'
  }
};

function Launcher({ route, theme, dispatch }) {
  const launcherStyles = route.isMaximising
    ? {
        ...styles.launcher,
        backgroundColor: theme.primaryColor,
        ...styles.launcherHiding
      }
    : {
        ...styles.launcher,
        backgroundColor: theme.primaryColor
      };

  return (
    <div onClick={() => dispatch(maximise())} style={launcherStyles}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden="true"
        focusable="false"
        data-prefix="fas"
        data-icon="comment"
        class="svg-inline--fa fa-comment fa-w-16"
        role="img"
        viewBox="0 0 512 512"
      >
        <path
          fill="currentColor"
          d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32z"
        />
      </svg>
    </div>
  );
}

const ConnectedLauncher = connect(state => ({
  route: state.route,
  theme: state.theme
}))(Launcher);
export default ConnectedLauncher;
