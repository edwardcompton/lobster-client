import { connect } from 'react-redux';

import { sendTextMessage } from '../reducers/chat';
import TypingIndicator from './widgets/TypingIndicator';
import Generic from './widgets/Generic';
import Avatar from './Avatar';
import QuickReply from './QuickReply';

const styles = {
  messageWrapper: {
    padding: '10px',
    animation: 'lobster-client--slide-up 150ms'
  },
  message: {
    display: 'flex'
  },
  userMessage: {
    justifyContent: 'flex-end'
  },
  bubble: {
    backgroundColor: 'white',
    maxWidth: '75%',
    width: 'fit-content',
    padding: '8px',
    borderRadius: '10px',
    transition: 'width 100ms, height 100ms'
  },
  userBubble: {
    paddingRight: '18px',
    paddingLeft: '18px',
    borderBottomRightRadius: '0px',
    backgroundColor: '#EEEEEE'
  },
  otherBubble: {
    paddingLeft: '18px',
    paddingRight: '18px',
    borderBottomLeftRadius: '0px',
    backgroundColor: '#EEEEEE',
    borderWidth: 2
  },
  systemBubble: {
    opacity: 0.8,
    fontSize: '0.9em'
  },
  bubbleText: {
    fontSize: '1em',
    lineHeight: '1.3em'
  },
  userName: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: '5px',
    fontSize: '0.9em',
    opacity: 0.5
  },
  avatar: {
    width: '1.1em',
    height: '1.1em',
    marginRight: '5px'
  },
  quickReply: {
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 2px',
    display: 'block',
    margin: '5px auto'
  }
};

export default function Message({ message, showReplies }) {
  switch (message.owner) {
    case 'system':
      return (
        <div style={styles.messageWrapper}>
          <div style={{ ...styles.message, justifyContent: 'center' }}>
            <div style={styles.systemBubble}>{message.value}</div>
          </div>
        </div>
      );
    case 'self':
      return <UserMessage type={message.type} value={message.value} />;
    default:
      return (
        <ConnectedOtherMessage
          type={message.type}
          value={message.value}
          owner={message.owner}
          replies={showReplies ? message.replies : undefined}
        />
      );
  }
}

function UserMessage(props) {
  let widget;
  switch (props.type) {
    case 'text':
    default:
      widget = (
        <div style={{ ...styles.bubble, ...styles.userBubble }}>
          <div style={{ ...styles.bubbleText, ...styles.userText }}>
            {props.value}
          </div>
        </div>
      );
  }

  return (
    <div style={styles.messageWrapper}>
      <div style={{ ...styles.message, ...styles.userMessage }}>{widget}</div>
    </div>
  );
}

function OtherMessage({ dispatch, type, value, owner, replies = [] }) {
  let widget;
  console.log(replies);

  switch (type) {
    case 'generic':
      widget = <Generic elements={value} />;
      break;
    case 'typing-indicator':
      widget = (
        <div style={{ ...styles.bubble, ...styles.otherBubble }}>
          <TypingIndicator />
        </div>
      );
      break;
    case 'text':
    default:
      widget = (
        <div style={{ ...styles.bubble, ...styles.otherBubble }}>
          <div style={styles.bubbleText}>{value}</div>
        </div>
      );
  }

  return (
    <div style={styles.messageWrapper}>
      <div style={styles.message}>{widget}</div>
      <div style={styles.userName}>
        <Avatar style={styles.avatar} />
        {owner}
      </div>
      {replies.map(reply => (
        <QuickReply
          title={reply.title}
          onClick={() => dispatch(sendTextMessage(reply.payload))}
          style={styles.quickReply}
        />
      ))}
    </div>
  );
}

const ConnectedOtherMessage = connect(() => ({}))(OtherMessage);
