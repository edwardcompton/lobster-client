import { connect } from 'react-redux';
import { createRef } from 'preact';
import { useEffect } from 'preact/hooks';
import useWindowSize from '../hooks/useWindowSize';

import ChatHeader from './ChatHeader';
import InputBar from './InputBar';
import Message from './Message';

const styles = {
  chatWindow: {
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    animation: 'lobster-client--slide-up 175ms'
  },
  chatWindowFixed: {
    position: 'fixed',
    height: '650px',
    width: '350px',
    bottom: '30px',
    right: '30px',
    boxShadow: 'rgba(0, 0, 0, 0.16) 0px 5px 40px',
    borderRadius: '6px'
  },
  chatWindowFullScreen: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: 0,
    right: 0
  },
  chatWindowHiding: {
    animation: 'lobster-client--slide-down 100ms',
    animationFillMode: 'forwards'
  },
  header: {
    boxShadow: '0 2px 1px rgba(0,0,0,0.2)'
  },
  chatPanel: {
    height: '100%',
    borderRight: '1px solid #EEEEEE',
    borderLeft: '1px solid #EEEEEE',
    overflowY: 'scroll'
  },
  scroll: {
    minHeight: '100%',
    overflowY: 'hidden',
    paddingBottom: '30px'
  }
};

function ChatWindow({ route, chat }) {
  const scrollEnd = createRef();
  useEffect(() => scrollEnd.current.scrollIntoView({ behavior: 'smooth' }));

  const isNarrow = useWindowSize().width < 480;
  const positioningStyles = isNarrow
    ? styles.chatWindowFullScreen
    : styles.chatWindowFixed;

  const chatWindowStyles = route.isMinimising
    ? {
        ...styles.chatWindow,
        ...positioningStyles,
        ...styles.chatWindowHiding
      }
    : {
        ...styles.chatWindow,
        ...positioningStyles
      };

  return (
    <div style={chatWindowStyles}>
      <ChatHeader />
      <div style={styles.chatPanel}>
        <div style={styles.scroll} onResize={console.log}>
          {chat.messages.map((message, index) => (
            <Message
              message={message}
              showReplies={index === chat.messages.length - 1}
            />
          ))}
          <div ref={scrollEnd}></div>
        </div>
      </div>
      <InputBar />
    </div>
  );
}

const ConnectedChatWindow = connect(state => ({
  route: state.route,
  chat: state.chat
}))(ChatWindow);
export default ConnectedChatWindow;
