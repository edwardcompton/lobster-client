import { useState } from 'preact/hooks';
import { connect } from 'react-redux';

import SendIcon from './SendIcon';
import { sendTextMessage } from '../reducers/chat';

const styles = {
  footer: {
    borderBottomLeftRadius: '6px',
    borderBottomRightRadius: '6px',
    border: '1px solid #EEEEEE'
  },
  mainFooter: {
    display: 'flex',
    alignItems: 'center',
    position: 'relative'
  },
  subFooter: {
    opacity: 0.5,
    textAlign: 'center',
    fontSize: '0.9em',
    padding: '6px'
  },
  inputSeparator: {
    position: 'absolute',
    width: 0,
    top: '-1px',
    left: '50%',
    height: '2px',
    transition: 'width 250ms, left 250ms'
  },
  inputSeparatorHighlighted: {
    width: '100%',
    left: 0
  },
  inputWrapper: {
    flex: 1
  },
  input: {
    width: '100%',
    border: 'none',
    padding: '20px'
  },
  sendButton: {
    cursor: 'pointer',
    margin: '10px',
    marginRight: '25px'
  },
  sendIcon: {
    height: '24px'
  }
};

function InputBar({ theme, style, dispatch }) {
  const [isFocused, setFocused] = useState(false);
  const [text, setText] = useState('');

  const inputSeparatorStyles = isFocused
    ? {
        ...styles.inputSeparator,
        ...styles.inputSeparatorHighlighted,
        backgroundColor: theme.primaryColor
      }
    : {
        ...styles.inputSeparator,
        backgroundColor: theme.primaryColor
      };

  const sendButtonColour = text === '' ? '#EEEEEE' : theme.primaryColor;

  function onKeyPress(event) {
    if (event.key === 'Enter' && text !== '') {
      submit();
    }
  }

  function submit() {
    dispatch(sendTextMessage(text));
    setText('');
  }

  return (
    <div
      style={{
        ...styles.footer,
        ...style
      }}
    >
      <div style={styles.mainFooter}>
        <div style={inputSeparatorStyles} />
        <div style={styles.inputWrapper}>
          <input
            style={styles.input}
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
            onChange={event => setText(event.target.value)}
            onKeyPress={onKeyPress}
            value={text}
            type="text"
            maxlength="512"
            placeholder="Your message..."
          />
        </div>
        <div onClick={submit} style={styles.sendButton}>
          <SendIcon style={styles.sendIcon} colour={sendButtonColour} />
        </div>
      </div>
      <div style={styles.subFooter}>
        We run on{' '}
        <a href="https://ebi.ai" target="_blank">
          Lobster
        </a>
      </div>
    </div>
  );
}

const ConnectedChatHeader = connect(state => ({ theme: state.theme }))(
  InputBar
);
export default ConnectedChatHeader;
