import useHover from '../hooks/useHover';

const styles = {
  button: {
    background: 'none',
    backgroundColor: 'rgba(0,0,0,0)',
    border: 'none',
    color: 'white',
    borderRadius: '50%',
    width: '2.2em',
    height: '2.2em',
    fontWeight: 'bold',
    transition: 'background-color 75ms',
    padding: '6px'
  },
  hoveredButton: {
    backgroundColor: 'rgba(0,0,0,0.5)'
  }
};

export default function ActionButton({ children, onClick }) {
  const [hoveredRef, isHovered] = useHover();

  const buttonStyles = isHovered
    ? { ...styles.button, ...styles.hoveredButton }
    : styles.button;

  return (
    <button onClick={onClick} ref={hoveredRef} style={buttonStyles}>
      {children}
    </button>
  );
}
