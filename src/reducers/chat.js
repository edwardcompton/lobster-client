const MINIMUM_RESPONSE_TIME = 2000; // milliseconds

export const CREATE_MESSAGE = 'lobster-client/chat/CREATE_MESSAGE';
export const CREATE_SELF_MESSAGE = 'lobster-client/chat/CREATE_SELF_MESSAGE';
export const ENABLE_TYPING_INDICATOR =
  'lobster-client/chat/ENABLE_TYPING_INDICATOR';
export const DISABLE_TYPING_INDICATOR =
  'lobster-client/chat/DISABLE_TYPING_INDICATOR';

export default function reduce(state = {}, action = {}) {
  const messages = state.messages || [];

  switch (action.type) {
    case CREATE_MESSAGE:
      return {
        ...state,
        messages: [...messages, action.message]
      };

    case CREATE_SELF_MESSAGE:
      action.message.owner = 'self';
      return {
        ...state,
        messages: [...messages, action.message]
      };

    case ENABLE_TYPING_INDICATOR:
      return {
        ...state,
        showTypingIndicator: true
      };

    case DISABLE_TYPING_INDICATOR:
      return {
        ...state,
        showTypingIndicator: false
      };

    default:
      return state;
  }
}

export function createMessage(message) {
  return {
    type: CREATE_MESSAGE,
    message
  };
}

export function createSelfMessage(message) {
  return {
    type: CREATE_SELF_MESSAGE,
    message
  };
}

export function enableTypingIndicator() {
  return {
    type: ENABLE_TYPING_INDICATOR
  };
}

export function disableTypingIndicator() {
  return {
    type: DISABLE_TYPING_INDICATOR
  };
}

export function sendTextMessage(text) {
  return (dispatch, getState, { socket }) => {
    const message = {
      type: 'text',
      value: text
    };
    socket.sendMessage(message);
    dispatch(createSelfMessage(message));
  };
}
