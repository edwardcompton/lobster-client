const MINIMISE_DURATION = 100; // milliseconds
const MAXIMISE_DURATION = 75; // milliseconds

export const MINIMISE = 'stina-playground/route/MINIMISE';
export const MINIMISING = 'lobster-client/route/MINIMISING';
export const MAXIMISE = 'lobster-client/route/MAXIMISE';
export const MAXIMISING = 'lobster-client/route/MAXIMISING';

export default function reduce(state = {}, action = {}) {
  const messages = state.messages || [];

  switch (action.type) {
    case MINIMISE:
      return {
        ...state,
        isMinimised: true,
        isMinimising: false
      };

    case MAXIMISE:
      return {
        ...state,
        isMinimised: false,
        isMaximising: false
      };

    case MINIMISING:
      return {
        ...state,
        isMinimising: true
      };

    case MAXIMISING:
      return {
        ...state,
        isMaximising: true
      };

    default:
      return state;
  }
}

export function minimise() {
  return dispatch => {
    console.log('isMinimising');
    dispatch({ type: MINIMISING });
    console.log('isMinimised');
    setTimeout(() => dispatch({ type: MINIMISE }), MINIMISE_DURATION);
  };
}

export function maximise() {
  return dispatch => {
    dispatch({ type: MAXIMISING });

    setTimeout(() => dispatch({ type: MAXIMISE }), MAXIMISE_DURATION);
  };
}
