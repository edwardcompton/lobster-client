import client from 'socket.io-client';

import { createMessage, userJoined } from '../reducers/chat';

export default function create(settings, services) {
  const { url } = settings.socket;
  const socket = client(url);
  socket.on('message', onMessage);

  let dispatch;

  return {
    setDispatch,
    sendMessage
  };

  function setDispatch(dispatchFunction) {
    dispatch = dispatchFunction;
  }

  function onMessage(message) {
    dispatch(createMessage(message));
  }

  function sendMessage(message) {
    console.log(message);
    socket.emit('message', message);
  }
}
