import './style';

import { combineReducers, createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import thunk from 'redux-thunk';

import socket from './services/socket';

import chatReducer from './reducers/chat';
import routeReducer from './reducers/route';

import Launcher from './components/Launcher';
import ChatWindow from './components/ChatWindow';

const rootReducer = combineReducers({
  theme: (state = {}) => state,
  chat: chatReducer,
  route: routeReducer
});

const startState = {
  theme: {
    primaryColor: '#2b79d9'
  },
  chat: {
    messages: [
      {
        owner: 'bot-1234',
        type: 'typing-indicator',
        value: 'Hello world!'
      },
      {
        owner: 'bot-1234',
        type: 'generic',
        value: [
          {
            title: 'Generic Element',
            content:
              'Cats, dogs and other small animals can travel in your car free of charge. Cats, dogs and other small animals can travel in your car free of charge.',
            buttons: [{ title: 'link', url: '#' }, { title: 'link', url: '#' }]
          }
          // {
          //   title: 'Generic Element 2',
          //   content:
          //     'Cats, dogs and other small animals can travel in your car free of charge. Cats, dogs and other small animals can travel in your car free of charge.'
          // }
        ]
      },
      {
        owner: 'bot-1234',
        type: 'generic',
        replies: [
          { title: 'yes, please!', payload: 'hi' },
          { title: 'no', payload: 'hi' }
        ],
        value: [
          {
            title: 'Generic Element',
            content:
              'Cats, dogs and other small animals can travel in your car free of charge. Cats, dogs and other small animals can travel in your car free of charge.',
            buttons: [{ title: 'link', url: '#' }, { title: 'link', url: '#' }]
          },
          {
            title: 'Generic Element 2',
            content:
              'Cats, dogs and other small animals can travel in your car free of charge. Cats, dogs and other small animals can travel in your car free of charge.'
          }
        ]
      }
    ]
  },
  route: {
    isMinimised: true
  }
};

const settings = {
  socket: {
    url: 'http://192.168.1.117:3000'
  }
};

const services = {};
services.socket = socket(settings, services);

const reduxMiddlewares = applyMiddleware(thunk.withExtraArgument(services));

const store = createStore(rootReducer, startState, reduxMiddlewares);
services.socket.setDispatch(store.dispatch);

function SimpleRouter({ route }) {
  if (route.isMinimised) {
    return <Launcher />;
  } else {
    return <ChatWindow />;
  }
}
const ConnectedSimpleRouter = connect(state => ({ route: state.route }))(
  SimpleRouter
);

export default function App() {
  return (
    <Provider store={store}>
      <ConnectedSimpleRouter />
    </Provider>
  );
}
